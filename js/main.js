
const themeBtn = document.querySelector('.theme');
const projectBtn = document.querySelector('.our_project_button');
const textSecondPage = document.querySelector('.second_page_content')
let elem = localStorage.getItem('theme');

theme(elem) ;

function theme (color){
    if (color === 'dark') {
        document.body.classList.remove('body-color');
        projectBtn.classList.remove('btn-color');
        textSecondPage.style.color = '#fff'; 
    } else if (color === 'light') {
        textSecondPage
        document.body.classList.add('body-color');
        projectBtn.classList.add('btn-color');
        textSecondPage.style.color = '#ec6e48'; 
    }
}

themeBtn.addEventListener('click', changeTheme);

function changeTheme(){
    if (elem === 'dark') {
        elem = 'light';
    }else {
        elem = 'dark';
    }
    theme(elem);
    localStorage.setItem('theme',elem)
    }



